# This file is a template, and might need editing before it works on your project.
FROM python:3

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
# RUN apk --no-cache add postgresql-client

WORKDIR /usr/src/app

COPY ./app /usr/src/app

#It will install the dependencies for the Flask application
RUN pip install --no-cache-dir -r requirements.txt

#It starts the Flask application on Port 5000
CMD python app.py
